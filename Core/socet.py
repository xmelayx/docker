import socket

sock = socket.socket()
sock.bind(('', 9090))
sock.listen(1)
conn, addr = sock.accept()
data = conn.recv(16384)
udata = data.decode("utf-8")
print('connected:', addr)
print("Data: " + udata)
if udata == "ls!":
    conn.send(b"Hallo")
else:
    print("none")

while True:
    data = conn.recv(1024)
    if not data:
        break
conn.close()